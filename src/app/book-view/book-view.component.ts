import { Component } from '@angular/core';
import { Ibook } from '../services/ibook';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../services/book.service';

@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.css'],
})
export class BookViewComponent {
  bgImage: string = '../../assets/images/book/header.svg';
  arrowImage: string = '../../assets/images/book/arrowBtn.svg';

  bookId!: number;
  books: Ibook[] = [];
  tempBook!: Ibook;

  constructor(
    private _bookService: BookService,
    private activatedRoute: ActivatedRoute
  ) {
    this.bookId = +this.activatedRoute.snapshot.paramMap.get('id')!;
    console.log('ID: ', this.bookId);
    this._bookService.getBooksByID(this.bookId).subscribe({
      next: (val) => {
        this.tempBook = val;
        console.log('val: ', val);
      },
    });
  }

  ngOnInit() {
    this._bookService.getBooks().subscribe({
      next: (book) => (this.books = book),
    });
  }
}
