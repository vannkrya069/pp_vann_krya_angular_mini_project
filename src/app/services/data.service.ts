import { Injectable } from '@angular/core';
import {
  InMemoryBackendConfig,
  InMemoryDbService,
} from 'angular-in-memory-web-api';
import { Ibook } from './ibook';
import { Iuser } from './iuser';

@Injectable({
  providedIn: 'root',
})
export class DataService implements InMemoryDbService {
  constructor() {}

  createDb() {
    // let auths: Iuser[] = [
    //   {
    //     id: 1,
    //     email: 'krya@gmail.com',
    //     password: '1234',
    //   },
    //   {
    //     id: 2,
    //     email: 'pmike@gmail.com',
    //     password: 'abcd',
    //   },
    //   {
    //     id: 3,
    //     email: 'john@gmail.com',
    //     password: 'abcd',
    //   },
    // ];

    let books: Ibook[] = [
      {
        id: 1,
        image: '../../assets/images/book/richPeople.svg',
        title: 'Rich People Problems',
        category: 'business',
      },
      {
        id: 2,
        image: '../../assets/images/book/longBright.svg',
        title: 'Long Bright River',
        category: 'science',
      },
      {
        id: 3,
        image: '../../assets/images/book/emberQueen.svg',
        title: 'Ember Queen',
        category: 'fiction',
      },
      {
        id: 4,
        image: '../../assets/images/book/followMe.svg',
        title: 'Follow Me To Ground',
        category: 'philosophy',
      },
      {
        id: 4,
        image: '../../assets/images/book/allTheNight.svg',
        title: 'All The Night',
        category: 'biography',
      },
      {
        id: 5,
        image: '../../assets/images/book/whereTheCrawdads.svg',
        title: 'Where The Crawdads Sing',
        category: 'business',
      },
    ];
    return { books };
  }
}
