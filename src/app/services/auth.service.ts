import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isAuthenticated = false;

  login() {
    this.isAuthenticated = true;
  }

  logout() {
    return this.isAuthenticated;
  }
}
