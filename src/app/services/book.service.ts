import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
import { Ibook } from './ibook';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  BASE_URL = 'api/books/';

  constructor(private http: HttpClient) { }

  getBooks() {
    return this.http.get<Ibook[]>(this.BASE_URL);
  }

  getBooksByID(id: number) {
    return this.http.get<Ibook>(this.BASE_URL + id);
  }

  deleteBook(id: number) {
    return this.http.delete(`${this.BASE_URL}${id}`);
  }
}
