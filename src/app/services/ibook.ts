export interface Ibook {
  id: number;
  image: string;
  title: string;
  category: string;
}
