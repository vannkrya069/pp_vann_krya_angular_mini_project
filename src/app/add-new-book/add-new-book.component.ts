import { Component } from '@angular/core';

@Component({
  selector: 'app-add-new-book',
  templateUrl: './add-new-book.component.html',
  styleUrls: ['./add-new-book.component.css']
})
export class AddNewBookComponent {
  
  bgImage: string = '../../assets/images/book/header.svg';
  arrowImage: string = '../../assets/images/book/arrowBtn.svg';
  bookImage: string = '../../assets/images/book/allTheNight.svg';

}
