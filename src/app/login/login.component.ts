import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  astronautImage: string = '../../assets/images/login/astronaut.svg';
  bookImage: string = '../../assets/images/login/book.svg';
  mailImage: string = '../../assets/images/login/mail.svg';
  lockImage: string = '../../assets/images/login/lock.svg';
  eyeOnImage: string = '../../assets/images/login/eye.svg';
  eyeOffImage: string = '../../assets/images/login/eye-off.svg';
  reactiveForm!: FormGroup;

  constructor(private _authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.reactiveForm = new FormGroup({
      emailPassword: new FormGroup({
        email: new FormControl('', [
          Validators.required,
          Validators.email,
          Validators.minLength(6),
          this.patternValidatorEmail(),
        ]),
        password: new FormControl('', Validators.required),
      }),
    });
  }

  patternValidatorEmail() {
    return Validators.pattern(
      /^[a-zA-Z0-9!@#$%^&*()_+{}\[\]:;<>,.?~]+@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}$/
    );
  }

  login() {
    this._authService.login();
    this.router.navigate(['/books']);
  }
}
