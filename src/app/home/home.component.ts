import { Component } from '@angular/core';
import { BookService } from '../services/book.service';
import { Ibook } from '../services/ibook';
import { tap } from 'rxjs';
import { Router } from '@angular/router';
// import bgHero from '../../assets/images/home/wave.svg'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  heroBg: string = '../../assets/images/book/imagehero.svg';

  books: Ibook[] = [];

  constructor(private _bookService: BookService, private router: Router) {}

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this._bookService.getBooks().subscribe(
      (book) => (this.books = book),
      (error) => console.log(error)
    );
  }

  deleteBook(id: number) {
    this._bookService.deleteBook(id).subscribe(() => {
      this.getBooks(), (error: any) => console.log(error);
    });
  }

  setBookId(bookId: number) {
    for (let book of this.books) {
      if (bookId === book.id) {
        this.router.navigate(['/books', bookId], {});
      }
    }
  }
}
