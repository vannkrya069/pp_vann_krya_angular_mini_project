import { Component } from '@angular/core';
import { BookService } from '../services/book.service';
import { Ibook } from '../services/ibook';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent {
  heroBg: string = '../../assets/images/book/imagehero.svg';

  books: Ibook[] = [];

  constructor(private _bookService: BookService, private router: Router) {}

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this._bookService.getBooks().subscribe(
      (book) => (this.books = book),
      (error) => console.log(error)
    );
  }

  deleteBook(id: number) {
    this._bookService.deleteBook(id).subscribe(() => {
      this.getBooks(), (error: any) => console.log(error);
    });
    console.log("Book deleted");
  }

  setBookId(bookId: number) {
    for (let book of this.books) {
      if (bookId === book.id) {
        this.router.navigate(['/books', bookId], {});
      }
    }
  }
}
