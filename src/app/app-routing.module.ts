import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { BookComponent } from './book/book.component';
import { BookViewComponent } from './book-view/book-view.component';
import { AddNewBookComponent } from './add-new-book/add-new-book.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'books', component: BookComponent, canActivate: [authGuard] },
  {
    path: 'books/:id',
    component: BookViewComponent,
  },
  {
    path: 'add-book',
    component: AddNewBookComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
