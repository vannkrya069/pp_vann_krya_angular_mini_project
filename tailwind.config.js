/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
     "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        // Configure your color palette here
        loginBgColor: '#1B3764'
      }
    },
    
  },
  plugins: [],
}

